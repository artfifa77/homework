word = input('Введіть ваше слово:')

condition = True
while condition:
    symbol_number = input('Введіть номер символу, починаючи з нуля:')
    if symbol_number.isdigit():  # Перевірка чи є змінна числом
        int_symbol_number = int(symbol_number)  # Створення змінної числового типу
        condition = False
    else:
        print('Потрібно вводити тільки числа')

# Перевірка чи не ввів користувач число більше за кіл-ть символів у слові
if len(word) >= int_symbol_number:
    symbol = word[int_symbol_number]  # Доставання символа з слова
    sentence = f'В слові "{word}" символ під номером {int_symbol_number} це "{symbol}"'
    print(sentence)
else:
    print ('Номер не повинен бути більшим за кількість символів в слові')





