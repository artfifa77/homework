# Напишіть функцію, що приймає два аргументи.

def constructor(arg1, arg2):
    # якщо аргументи відносяться до числових типів - повернути різницю цих аргументів
    if (type(arg1) is int or type(arg1) is float) and (type(arg2) is int or type(arg2) is float):
        return arg1 - arg2

    # якщо обидва аргументи це строки - обʼєднати в одну строку та повернути
    elif type(arg1) is str and type(arg2) is str:
        return arg1 + arg2

    # якщо перший строка, а другий ні - повернути dict де ключ це перший аргумент, а значення - другий
    elif type(arg1) is str and type(arg2) is not str:
        args_dict = {arg1: arg2}
        return args_dict

    # у будь-якому іншому випадку повернути кортеж з цих аргументів
    else:
        return arg1, arg2


res = constructor('aa', 'a')
print(res)
