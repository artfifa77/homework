# Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається функція має повернути 0.


def float_maker(arg1):
    try:
        return float(arg1)
    except ValueError:
        return 0


res = float_maker(11)
print(res)
