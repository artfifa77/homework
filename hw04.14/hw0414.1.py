# Дана довільна строка.
# Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.


# Програма працює тільки на англійскій мові
words = input('Введіть англійські слова: ')

splitted_words = words.split()

vowels = ['a', 'o', 'i', 'e', 'u', 'y']
words_with_double_vowels = []

for word in splitted_words:
    letter_counter = 0
    for letter in word:
        if letter in vowels:
            letter_counter += 1
        if letter_counter == 2:
            words_with_double_vowels.append(word)
        if letter not in vowels:
            letter_counter = 0
if words_with_double_vowels == 0:
    print('Таких слів немає')
else:
    print(len(words_with_double_vowels))
