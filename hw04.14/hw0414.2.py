# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами:
# { "cito": 47.999 ... "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів,
# ціни яких попадають в діапазон між мінімальною і максимальною ціною.

stores_dict = {
    'cito': 47.999,
    'BB_studio': 42.999,
    'momo': 49.999,
    'main-service': 37.245,
    'buy.now': 38.324,
    'x-store': 37.166,
    'the_partner': 38.988,
    'sota': 37.720,
    'rozetka': 38.003}

list_of_stores = []

lower_price = 37.720
upper_price = 40

# Перевірка даних
if lower_price > upper_price:
    print('Некоректні дані')
elif lower_price < 0 or upper_price < 0:
    print('Некоректні дані')
else:
    for key, values in stores_dict.items():
        if lower_price <= values <= upper_price:
            list_of_stores.append(key)

if not list_of_stores:
    print('Немає результатів')
else:
    print(f'Магазини з вказаною ціною: {list_of_stores}')
