# 1. Задача: Створіть дві змінні first=10, second=30.
# Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.

first = 10
second = 30

result = first + second
print(result)

result = first - second
print(result)

result = first / second
print(result)

result = first * second
print(result)

result = first ** second
print(result)

result = first % second
print(result)

# 2. Задача: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
# Виведіть на екран результат кожного порівняння.

result = 40 > -20
print(result)

result = 0.3333333333333333 < -20
print(result)

result = 1000000000000000000000000000000 != 300
print(result)

result = 10 == 300
print(result)

# 3. Задача: Створіть змінну - результат конкатенації (складання) строк str1="Hello " и str2="world".
# Виведіть на екран.

str1 = 'Hello '
str2 = 'world'
string_result = str1 + str2
print(string_result)